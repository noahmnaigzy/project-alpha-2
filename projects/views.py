from django.shortcuts import render, get_object_or_404
from .models import Project
from django.views.generic import ListView
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import logout
from .forms import ProjectForm


# Create your views here.


def redirect_to_project_list(request):
    return redirect(reverse("list_projects"))


@method_decorator(login_required, name="dispatch")
class ProjectListView(ListView):
    model = Project
    template_name = "projects/project_list.html"
    context_object_name = "projects"

    def get_queryset(self):
        return Project.objects.filter(owner=self.request.user)


def logout_view(request):
    logout(request)
    return redirect("login")


@login_required
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect(
                "list_projects"
            )  # Redirect to the project list view
    else:
        form = ProjectForm()
    return render(request, "projects/project_create.html", {"form": form})
