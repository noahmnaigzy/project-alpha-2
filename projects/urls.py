from django.urls import path
from .views import ProjectListView
from . import views

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("logout/", views.logout_view, name="logout"),
    path("<int:id>/", views.project_detail_view, name="show_project"),
    path("create/", views.create_project_view, name="create_project"),
]
