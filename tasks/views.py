from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task

# Create your views here.


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/task_create.html", {"form": form})


@login_required
def show_my_tasks_view(request):
    user = request.user
    assigned_tasks = Task.objects.filter(assignee=user)
    context = {
        "assigned_tasks": assigned_tasks,
    }
    return render(request, "tasks/my_tasks.html", context)
