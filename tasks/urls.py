from django.urls import path
from . import views

urlpatterns = [
    path("create/", views.create_task_view, name="create_task"),
    path("mine/", views.show_my_tasks_view, name="show_my_tasks"),
]
